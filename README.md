# QuinApi technical case

The task is to create a series of automated tests that cover the CRUD operations of jsonbin API. The documentation page here: https://jsonbin.io/api-reference (Bins API). 

API test cases were automated using OOP concepts. My framework is built using TestNG with Rest Assured library, Jackson-databind (POJO), Lombok(POJO) libraries, and Java programming language. It contains the automation codes of the API test cases (POST, PUT, GET, DELETE). I have created positive test case scenarios with CRUD operations and negative test case scenarios. 
BaseURI, basePath, key, token and invalid token are stored in the configuration properties file. BeforeClass and AfterClass methods are stored in TestBase class. I have created a smokeTestRunner.xml file to run important tests (can be done in parallel testing if desired). 

## Instructions on how to execute tests

I have implemeted different types of assertion specifically for the sake of assignment and to show my skills, normally I would use one type of assertion consistently, here I have used Hamcrest mathers, pojo testing, Path method assertion. First, in positive scenarios classes, a bin is created using the POST command (createBin test case), the newly created bin is called using GET command (readBin test case), then bin is edited using PUT command (updateBin test case), then the bin is deleted using DELETE command (deleteBin test case), and after the bin was deleted next test scenario checks if it was indeed deleted using GET command (BinAfterDeleted test case). There are also JSON Schema Assertion(JsonSchemaAssertion test case) and POJO assertion (pojoAssertion test case). I wasn’t provided with Jason Schema so I have created it myself and asserted test cases against it. Getters and Setters auto-generated in POJO classes with the help of the LOMBOK library. Jackson-databind library was necessary to ignore the java reserved word “private” for the POJO class. 

In the negative scenario class, a first test case (InvalidKey test case), sends a valid post body with an invalid token to verify if it returns an error message. noKey test case sends valid body with no token to verify error message. The next test case (InvalidID test case) sends a valid body with an invalid id as a path parameter, to assert that it returns an error message. Finally, the noID test case sends a valid body and authorization without any ID as a path parameter. 

### Things I would add if I had more time

Maybe I would add more negative scenarios, but I was not sure if it was neccessery. And I would look more into CI/CD pipeline and figure it to work on GitLab.  




